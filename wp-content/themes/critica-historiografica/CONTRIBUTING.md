# Como contribuir para o projeto Crítica Historiográfica
Olá! Contribuições são bem-vindas!

Por favor, leia o guia de contribuição antes de fazer alterações.

A seguir descreveremos as formas mais adequadas de contribuir para o desenvolvimento e melhorias do projeto

## Contribuição com revisão e melhrorias no código

Sugerismo instalar a pilha LAMP em ambiente de localhost (sua máquina) dentro de contêineres

### Montando o ambiente de desenvolvimento
Baixando a lista atualizada de pacotes e códigos fontes de software da distribuição Debian / Ubuntu / outras para uso pelo gerenciador de pacotes apt (salvo no arquivo /etc/apt/source.list):
```Bash
sudo apt-get update
```
Instalando softwares utilitários do sistema GNU dos quais o Docker depende para funcionar, o Git e outros softwares necessários:
```Bash
sudo apt-get -y install \
   ca-certificates \
   curl \
   gnupg \
   lsb-release \
   git \
   wget \
   unzip
```
Baixando e configurando as chaves de verificação de autenticidade do pacote Docker Engine:
```Bash
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
```
Baixando e salvando o link para o repositório oficial dos pacotes e fontes do Docker Engine para uso pelo gerenciador de pacotes apt (salvo no arquivo /etc/apt/source.list.d/docker.list):
```Bash
sudo apt-get update
```
Insatalando o Docker Engine, seu plugin Docker Compose e Interface de Linha de Comando (CLI):
```Bash
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
```
Clonando o repositório da pilha Docker-compose-LAMP no diretório de projetos em sua máquina (altere o nome do diretório de trabalho caso seja diferente de 'projetos' ou inicie com maiúscula):
```Bash
cd ~/projetos && \
git clone https://github.com/sprintcube/docker-compose-lamp.git &&\
mv docker-compose-lamp/ lamp/ &&\
cd lamp/ &&\
cp sample.env .env &&\
docker-compose up -d
```
Clone o repositório do projeto `Crítica Historiográfica` no diretório `www/`:
```Bash
cd www/ && \
https://gitlab.com/coletivofarpa/critica-historiografica.git
```
Siga as instruções do arquivo [README.md](./README.md)