// ./src/index.js é o "entry point". Isso significa que o Webpack começará a compilar a partir deste arquivo, e o resultado da compilação será salvo como bundle.js no diretório dist.
const path = require('path');

module.exports = {
    entry: './src/js/index.js', // Caminho para o arquivo JavaScript principal
    output: {
        filename: 'bundle.js', // Arquivo com o resultado/saída da compilação
        path: path.resolve(__dirname, 'dist'), // Diretório de destino/saída
        publicPath: '/dist/' // Caminho público para o diretório de saída
    },
    module: {
        rules: [
            {
                test: /\.scss$/, // Expressão regular para arquivos Sass
                use: [
                    'style-loader', // Carrega estilos CSS no DOM
                    'css-loader', // Converte CSS em módulos CommonJS
                    'sass-loader' // Compila Sass em CSS
                ]
            }
        ]
    }
};