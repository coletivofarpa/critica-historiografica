<?php
/*
 * Theme Name: Crítica Historiográfica
 * Theme URI: https://gitlab.com/coletivofarpa/critica-historiografica
 * Description: Um tema personalizado para WordPress.
 * Author: Coletivo Farpa
 * Author URI: http://coletivofarpa.org
 * License: GPL-3.0-or-later
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

    get_header(); ?>

    <main id="main">
        
        <h1><?php echo get_bloginfo('name'); ?></h1> <!-- Título da página principal -->

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class('card mb-3'); ?> role="article">
                <header class="article-header card-header">
                    <h2 class="card-title"><?php the_title(); ?></h2>
                    <hr>
                    <?php if (has_post_thumbnail()) : ?>
                        <div class="post-thumbnail">
                            <?php the_post_thumbnail('thumbnail', ['class' => 'card-img-top']); ?> <!-- Exemplo de exibição da imagem destacada da postagem -->
                        </div>
                    <?php endif; ?>
                    <p class="card-subtitle mb-2 text-muted">Data: <?php the_date(); ?></p> <!-- Exemplo de exibição da data da postagem -->
                </header>
                <div class="card-body">
                    <p class="card-text"><?php the_excerpt(); ?></p> <!-- Exemplo de exibição do resumo da postagem -->
                </div>
                <footer class="card-footer">
                    <?php if (has_tag()) : ?>
                        <div class="post-tags">
                            <?php the_tags('<span class="badge bg-secondary">Palavras-chave: ', '</span><span class="badge bg-secondary"> ', '</span>'); ?> <!-- Exemplo de exibição das palavras-chave da postagem -->
                        </div>
                    <?php endif; ?>
                </footer>
            </article>
        <?php endwhile; endif; ?>
    </main>
 
    <?php get_footer(); ?>