        <footer class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
                    </div>
                </div>
            </div>
        </footer>

        <?php wp_footer(); ?>
    </body>
</html>