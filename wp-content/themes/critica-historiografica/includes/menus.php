<?php
// Registro de menus de navegação
function registrar_menu_principal() {
    register_nav_menus(array(
        'menu-principal' => __('Menu Principal', 'critica-historiografica'),

        // Adicione mais menus acima conforme necessário
    ));
}
add_action('init', 'registrar_menu_principal');