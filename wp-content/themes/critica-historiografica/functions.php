<?php
/*
* Author: Coletivo Farpa
* URL: http://coletivofarpa.org
*
* É aqui que você pode inserir suas funções personalizadas ou apenas editar coisas como tamanhos de miniaturas, imagens de cabeçalho, barras laterais, comentários, etc.
* This is where you can drop your custom functions or just edit things like thumbnail sizes, header images, sidebars, comments, etc.
*/

// Incluir o arquivo de funcionalidade do tema
// Include theme functionality file
// CARREGAR NÚCLEO CRÍTICO (se você remover isso, o tema irá quebrar)
// LOAD CRITICAL CORE (if you remove this, the theme will break)
require get_template_directory() . '/includes/funcoes-criticas.php';

/* NÃO EXCLUA ESSA TAG DE FECHAMENTO | DON'T DELETE THIS CLOSING TAG */ ?>