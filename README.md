# 1. Crítica Historiográfica
Crítica Historiográfica (ISSN 2764-2666) é uma publicação mantida por grupos de pesquisa em História sediados na Universidade Federal do Rio Grande do Norte (UFRN), na Universidade federal de Sergipe (UFS) e Universidade Regional do Cariri (URCA). Nossa missão é publicar artigos de revisão, resenhas de livros de História e relatórios de bibliografia histórica. Domínio: https://criticahistoriográfica.com.br

| 🇪🇸 | 🇬🇧 |

## 2. Instalação

1. Clone este repositório com Git ou Git Bash.
```Bash
git clone https://gitlab.com/coletivofarpa/critica-historiografica.git
```
2. Instale o WordPress no diretório raíz deste projeto:
```Bash
wget https://wordpress.org/latest.zip && \
unzip latest.zip && \
rm -r wordpress/wp-content/ && \
mv wordpress/* ./ && \
rmdir wordpress/ && \
rm latest.zip
```
3. Execute `npm install` para instalar as dependências.
```Bash
npm install
```
4. Execute `npm run build` para construir o projeto.

```Bash
npm run build
```
5. Compile os arquivos `*.scss` para gerar o arquivo `estilos.css`. Abra o terminal no diretório `src/` e execute o comando referente ao seu sistema operaticional:
GNU com ou sem Linux
```Bash
sass --watch src/scss/estilos.scss:src/css/estilos.css --style compressed
```
Rwindows com Git Bash instalado
```Bash
sass src/scss/estilos.scss src/css/estilos.css --watch compressed
```

## 3. Como Usar
```javascript
import { meuFuncao } from 'meu-projeto';

// Use minhaFuncao
```
## 4. **Contribuição**: Indique como os outros podem contribuir para o seu projeto.


### Contribuição

Contribuições são bem-vindas! Por favor, leia o [guia de contribuição](./wp-content/themes/critica-historiografica/CONTRIBUTING.md) antes de fazer alterações.

## Licença

Este projeto está licenciado sob a [GPL-3.0-or-later](LICENSE).

Um guia rápido para a [GPLv3](https://www.gnu.org/licenses/quick-guide-gplv3.html)

Fonte original da [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt)

## Créditos

Agradecimentos especiais a [Nome do Contribuidor](https://github.com/contribuidor) por sua contribuição.